package tdp.base.item;

import java.util.*;
//import tdp.base.item.*;

public class Bag {
	private ArrayList<Item> inventory;
	private int slots;

	// default constructor assuming 10 slots
	public Bag() {
		this.slots = 5;
		this.inventory = new ArrayList<Item>(this.slots);
	}

	public Bag(int maxSlots) {
		this.slots = maxSlots;
		this.inventory = new ArrayList<Item>(maxSlots);
	}

	public Bag(Bag previous) {
		this.slots = previous.slots;
		this.inventory = new ArrayList<Item>(previous.inventory);
		//this.inventory = previous.inventory;
			// shallow copy might fit better for 'graduate' procedure
			// since you 'graduate'/'job change' not 'clone' yourself
			// but i feels like this makes a bit more sense when 
			// object separation is a concern
		/* sort of deeper bag cloning?
		this.inventory = new ArrayList<Item>(previous.slots);
		for (Item item : previous.inventory) {
			this.inventory.add(item);
		}
		*/
	}

	public Bag copyBag() {
		return new Bag(this);
	}

	public int size() { return this.inventory.size(); }
	public int getSlots() { return this.slots; }
	public boolean isVacant() { return this.size() < this.slots; }
	public ArrayList<Item> getRawListings() { return this.inventory; }

	/**
	 * search the bag for particular item
	 * 
	 * @param itemName name of an item to find
	 * @return number of found count, 0 if none exist
	 */
	public int searchInBag(String itemName) {
		int count = 0;
		for(Item item : this.inventory) {
			if (item.getName().toLowerCase()
				.equals(itemName.toLowerCase())) {
				count++;
			}
		}
		return count;
	}

	public boolean isInBag(String itemName) {
		return (this.searchInBag(itemName) > 0);
	}

	/**
	 * attemps to put an item into inventory
	 *  
	 * @param item Item to be put in
	 * @return putting item into bag success, fail if full slots
	 */
	public boolean putItem(Item item) {
		// bag must have empty slot
		if (this.size() >= this.slots) {
			return false;
		}

		// put this in if not exist. multiple copies are alllowed
		this.inventory.add(item);
		return true;
	}

	/**
	 * attempts to pull out specified item in the bag
	 * pulls the first one it finds
	 * 
	 * @param itemName item name to get from the bag
	 * @return Item if exist, null if not
	 */
	public Item getItem(String itemName) {
		if (!this.isInBag(itemName)) {
			return null;
		}
		
		Item retrieved = null;
		for (Item it : this.inventory) {
			if (it.getName().toLowerCase()
				.equals(itemName.toLowerCase())) {
				retrieved = it;
				this.inventory.remove(it);
				break;
			}
		}

		return retrieved;
	}

	/**
	 * inspect an given item
	 *
	 * @param item item to be instected
	 */
	public void inspectItem(Item item) {
		if (this.isInBag(item.getName())) {
			item.describe();
		}
	}

	public void listItems() {
		for (Item it : this.inventory) {
			this.inspectItem(it);
			System.out.println();
		}
	}

	/**
	 * randomly pulled an item from this bag, if such items exist
	 * 
	 * @return a randomly picked Item, or null if bag is empty.
	 */
	public Item pullRandomItem() {
		Random rng = new Random();
		Item pulled = null;
		if (this.size() > 0) {
			int rngindex = rng.nextInt(this.size());
			pulled = this.inventory.get(rngindex);
			this.inventory.remove(rngindex);
		}
		return pulled;
	}
} 
