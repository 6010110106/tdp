import java.util.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import tdp.asset.*;
import tdp.base.student.*;
import tdp.base.remnant.*;
import tdp.base.item.*;
import tdp.base.remnant.RemnantMastermind;
import tdp.base.combat.*;
import tdp.ui.*;

public class PartyUI extends JFrame {
	Container cont;
	JPanel infoPanel;
	JPanel centerPanel;

	StudentActionPanel actionPanel;
	BattlePanel currentBattle;
	
	GridBagConstraints infoConst;

	ArrayList<StudentInfoPanel> studentInfos;
	ArrayList<JRadioButton> studentSelectors;
	ArrayList<JPanel> studentSelectablePanels;

	ButtonGroup protagSelector;

	ArrayList<Student> students;

	Student protag;

	public PartyUI() {
		super("TDP");
		this.cont = this.getContentPane();
		this.cont.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.protag = null;

		this.studentInfos = new ArrayList<>();
		this.studentSelectablePanels = new ArrayList<>();
		this.studentSelectors = new ArrayList<>();

		this.infoPanel = new JPanel();
		this.infoPanel.setLayout(
			new GridBagLayout()
		);

		this.infoConst = new GridBagConstraints();
		this.infoConst.weightx = 1;
		this.infoConst.fill = GridBagConstraints.HORIZONTAL;
		this.infoConst.gridwidth = GridBagConstraints.REMAINDER;

		this.actionPanel = new StudentActionPanel();
		this.actionPanel.setPreferredSize(new Dimension(180, 0));
		
		this.currentBattle = new BattlePanel();
		this.currentBattle.setVisible(false);

		this.protagSelector = new ButtonGroup();

		this.centerPanel = new JPanel();
		this.centerPanel.setLayout(new BorderLayout());

		this.centerPanel.add(this.infoPanel, BorderLayout.CENTER);
		this.centerPanel.add(this.actionPanel, BorderLayout.EAST);

		this.cont.add(Box.createHorizontalStrut(640), BorderLayout.CENTER);
		this.cont.add(this.centerPanel, BorderLayout.CENTER);

		this.setSize(680, 520);
		this.setResizable(true);
		//this.setVisible(true);
	}

	public void setActiveStudent(Student player) {
		this.protag = player;
	}

	public void addStudent(Student student) {
		// add new info display
		StudentInfoPanel newStudent = new StudentInfoPanel(student);
		this.studentInfos.add(newStudent);

		// add new selector to select this student as 'protag'
		class StudentSelector extends JRadioButton implements ActionListener {
			Student target;
			public StudentSelector(Student student) {
				super();
				super.setActionCommand(student.getName());
				super.addActionListener(this);
				this.target = student;
			}

			public void setStudent(Student student) { this.target = student; }
			public Student getStudent() { return this.target; }

			@Override
			public void actionPerformed(ActionEvent e) {
				PartyUI self = PartyUI.this;
				protag = this.target;
				actionPanel.setStudent(protag);
				updatePanels();

				boolean alive = protag.isAlive();
				self.actionPanel.reviveButton.setEnabled(!alive);
			}
		}

		StudentSelector selector = new StudentSelector(student);
		this.protagSelector.add(selector);
		this.studentSelectors.add(selector);

		JPanel newSelectable = new JPanel(new BorderLayout());
		newSelectable.add(selector, BorderLayout.WEST);
		newSelectable.add(newStudent, BorderLayout.CENTER);
		//newSelectable.add(Box.createHorizontalGlue(), BorderLayout.EAST);
		newSelectable.setAlignmentX(Component.LEFT_ALIGNMENT);

		this.studentSelectablePanels.add(newSelectable);
		
		if (this.protag == null) {
			this.setActiveStudent(student);
			selector.setSelected(true);
			this.actionPanel.setStudent(student);
		}

	}

	public void addStudentPanels() {
		for (JPanel panel : this.studentSelectablePanels) {
			this.infoPanel.add(panel, this.infoConst);
		}
		//this.pack();
	}

	void updatePanels() {
		for (StudentInfoPanel sip : this.studentInfos) {
			sip.updateStudentInfo();
		}
		boolean alive = this.protag.isAlive();
		this.actionPanel.reviveButton.setEnabled(!alive);
		this.actionPanel.initiateBattle.setEnabled(alive);
		this.actionPanel.updateInventoryListings();
	}

	void repack() {
		this.pack();
	}

	public static void main(String[] args) {
		final String lookAndFeel = 
			"com.sun.java.swing.plaf.motif.MotifLookAndFeel";

		Student naegi = new Student("Makoto Naegi", 30, 30);
		Student hinata = new SportStudent("Hajime Hinata", 35, 25);
		Student akamatsu = new IntellectStudent("Kaede Akamatsu", 25, 40);

		naegi.setIconResource("/tdp/asset/icons/naegi.png");
		hinata.setIconResource("/tdp/asset/icons/hinata.png");
		akamatsu.setIconResource("/tdp/asset/icons/akamatsu.png");

		try {
            // Set System L&F
            UIManager.setLookAndFeel(lookAndFeel);
		} 
		catch (UnsupportedLookAndFeelException e) {
			// handle exception
		}
		catch (ClassNotFoundException e) {
			// handle exception
		}
		catch (InstantiationException e) {
			// handle exception
		}
		catch (IllegalAccessException e) {
			// handle exception
		}


        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
				PartyUI studentTest = new PartyUI();
				studentTest.addStudent(naegi);
				studentTest.addStudent(hinata);
				studentTest.addStudent(akamatsu);
				studentTest.addStudentPanels();

				studentTest.setVisible(true);
            }
        });
	}

	class StudentActionPanel extends JPanel {
		private Student student;

		private JPanel buttonsPanel;
		private JPanel bagPanel;

		private GridBagConstraints buttConst;

		private DefaultListModel<Item> bagListModel;
		private JList<Item> bagList;

		private TitledBorder buttonsBorder;
		private TitledBorder bagListBorder;

		private JButton initiateBattle;
		private JButton getItem;
		private JButton useItem;
		private JButton discardItem;
		private JButton inspectItem;
		private JButton reviveButton;

		public StudentActionPanel(Student student) {
			this();
			this.setStudent(student);
		}

		public StudentActionPanel() {
			super();

			this.setLayout(new GridLayout(0, 1));
			
			this.buttonsPanel = new JPanel();
			this.buttonsPanel.setLayout(
				new GridBagLayout()
			);
			
			this.buttConst = new GridBagConstraints();
			this.buttConst.weightx = 1;
			this.buttConst.fill = GridBagConstraints.HORIZONTAL;
			this.buttConst.gridwidth = GridBagConstraints.REMAINDER;

			this.buttonsBorder = new TitledBorder("Actions");
			this.buttonsPanel.setBorder(this.buttonsBorder);

			this.bagPanel = new JPanel(new FlowLayout());
			this.bagListBorder = new TitledBorder("Inventory");
			this.bagPanel.setBorder(this.bagListBorder);

			this.initiateBattle = new JButton("Battle!");
			this.initiateBattle.addActionListener(new ActionListener(){
				RemnantMastermind mastermind = new RemnantMastermind();
				StudentActionPanel self = StudentActionPanel.this;
				BattlePanel battPanel = PartyUI.this.currentBattle;
				@Override
				public void actionPerformed(ActionEvent e) {
					if (self.student.isAlive()) {
						this.startBattle(e);
					}
					else {
						JOptionPane.showMessageDialog(
							null, 
							String.format(
								"%s is out, unable to initiale battle.",
								self.student
							)
						);
						self.updateAll();
					}
				}

				public void startBattle(ActionEvent e) {
					Remnant remnant = mastermind.birthRemnant(self.student);
					battPanel = new BattlePanel();
					battPanel.setCombatants(remnant, self.student);

					JFrame newBattle = new JFrame("Battle");
					newBattle.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
					newBattle.addWindowListener(new WindowListener() {
						@Override
						public void windowClosing(WindowEvent e) {
							if (battPanel.isCombatantsAlive()) {
								JOptionPane.showMessageDialog(
									newBattle, 
									"There's nowhere to run...", 
									"Upupupupu", 
									JOptionPane.WARNING_MESSAGE
								);
							}
							else {
								newBattle.dispose();
							}
							self.updateAll();
							PartyUI.this.updatePanels();
						}
						@Override
						public void windowClosed(WindowEvent e) {
							self.updateAll();
							PartyUI.this.updatePanels();
						}

						@Override
						public void windowOpened(WindowEvent e) {}
						@Override
						public void windowIconified(WindowEvent e) {}
						@Override
						public void windowDeiconified(WindowEvent e) {}
						@Override
						public void windowDeactivated(WindowEvent e) {}
						@Override
						public void windowActivated(WindowEvent e) {}
					});
					newBattle.getContentPane().add(battPanel);
					newBattle.pack();
					newBattle.setVisible(true);
				}
			});

			this.reviveButton  = new JButton("Revive");
			this.reviveButton.setEnabled(false);
			this.reviveButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					boolean success = student.revive();
					if (success) {
						reviveButton.setEnabled(false);
						updatePanels();
					}
				}
			});

			this.getItem = new JButton("Scavenge");
			this.getItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (student.isBagVacant()) {
						scavenge();
					}
					else {
						JOptionPane.showMessageDialog(
							null, 
							student.getName() + "'s bag is full."
						);
					}
				}

				public void scavenge() {
					ItemServer its = new ItemServer();
					Random rng = new Random();
					double rand = rng.nextDouble();
					if (rand < 0.8) {
						Item picked = its.getRandomItem();
						student.putItem(picked);
						student.gainExp(rng.nextDouble()*5);
					}
					StudentActionPanel.this.updateAll();
				}
			});
			
			this.useItem = new JButton("Use Item");
			this.useItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Student is = StudentActionPanel.this.student;
					Item select = bagList.getSelectedValue();
					if (select != null) {
						String pendingName = select.getName();
						System.out.println(pendingName);
						if (select instanceof ConsumableItem
							|| select instanceof ApparelItem
							|| select instanceof GearItem ) {
							Item pending = is.pullItem(pendingName);
							if (!is.useItem(pending)) {
								is.putItem(pending);
							}
						}
						else {
							JOptionPane.showMessageDialog(
								null, select.describe()
							);
						}
					}
					StudentActionPanel.this.updateAll();
				}
			});

			this.inspectItem = new JButton("Inspect");
			this.inspectItem.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					if (bagList.getSelectedValue() != null) {
						JOptionPane.showMessageDialog(
							null,
							bagList.getSelectedValue().describe()
						);
					}
				}
			});

			this.discardItem = new JButton("Discard Item");
			this.discardItem.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					Student is = StudentActionPanel.this.student;
					if (bagList.getSelectedValue() != null) {
						is.pullItem(bagList.getSelectedValue().getName());
					}
					StudentActionPanel.this.updateAll();
				}
			});

			this.bagListModel = new DefaultListModel<Item>();
			this.bagList = new JList<Item>();
			this.bagList.setAlignmentX(Container.LEFT_ALIGNMENT);

			this.buttonsPanel.add(this.initiateBattle, this.buttConst);
			this.buttonsPanel.add(this.getItem, this.buttConst);
			this.buttonsPanel.add(this.inspectItem, this.buttConst);
			this.buttonsPanel.add(this.useItem, this.buttConst);
			this.buttonsPanel.add(this.discardItem, this.buttConst);
			this.buttonsPanel.add(this.reviveButton, this.buttConst);

			this.bagPanel.add(this.bagList);

			this.add(this.bagPanel);
			this.add(this.buttonsPanel);
		}

		public void updateInventoryListings() {
			this.bagListModel.clear();
			for (Item it : this.student.getInventoryList()) {
				this.bagListModel.addElement(it);
			}
			this.bagList.setModel(this.bagListModel);
		}

		public void updateAll() {
			StudentActionPanel self = StudentActionPanel.this;
			//self.initiateBattle.setEnabled(this.student.isAlive());
			self.updateInventoryListings();
			PartyUI.this.updatePanels();
		}

		public Student getStudent() { return this.student; }

		public void setStudent(Student student) {
			this.student = student;
			this.updateInventoryListings();
		}
	}
}
