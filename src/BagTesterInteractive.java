import java.util.*;

import tdp.asset.*;
import tdp.base.combat.*;
import tdp.base.item.*;
import tdp.base.student.*;

public class BagTesterInteractive {
	// shitty bag testing system, for better testing check out
	// `BagTester.java`
	// not anymore?
	public static void main(String[] args) {
		Random rand = new Random();
		ItemServer its = new ItemServer();
		ArrayList<Item> itemServer = its.getList();
		Scanner scanner = new Scanner(System.in);
		String input;
		ArrayList<Student> students = new ArrayList<>();
		Student hinata = new Student(
			"Hinata Hajime", 30, 20);
		SportStudent owari = new SportStudent(
			"Owari Akane", 35, 20);
		IntellectStudent nanami = new IntellectStudent(
			"Nanami Chiaki", 22, 35);
		students.add(hinata);
		students.add(owari);
		students.add(nanami);
		Student player = hinata;

		NewBattleAction scene = null;
		
		do {
			if (scene != null) {
				System.out.println("Currently in battle.");
			}
			System.out.print("cmd> ");
			input = scanner.nextLine().trim();
			String action[] = input.split(" ", 2);
			action[0] = action[0].toLowerCase();
			
			if (action[0].equals("walk")) {
				if (action.length != 2) {
					System.out.println("E: No step specified.");
					continue;
				}
				int steps = Integer.parseInt(action[1]);
				player.gainExp(expFromWalking(steps));

				// random chance of getting new item
				if (rand.nextDouble() < (0.3 + 0.005*steps)) {
					System.out.printf("%s found some items!\n", 
					player.getName());
					player.putItem(
						itemServer.get(rand.nextInt(itemServer.size()))
					);
				}

				// random chance of hurt self
				if (rand.nextDouble() < (0.2 + 0.005*steps)) {
					int dmg = (int)(rand.nextDouble() * player.getHealth()/2.0);
					System.out.printf("%s tripped!\n", player.getName());
					player.takeDamage(dmg);
				}
			}
			else if (action[0].equals("switch")) {
				if (action.length == 2) {
					boolean switchSuccess = false;
					for (Student std: students) {
						if (std.getName().toLowerCase()
							.contains(action[1])) {
							player = std;
							switchSuccess = true;
							break;
						}
					}
					if (switchSuccess) {
						System.out.printf("Now assuming control of %s\n",
							player.getName());
						System.out.printf("Student type: %s\n",
							player.getStyle());
					}
				}
				else {
					System.out.printf("E: no switch target");
				}
			}
			else if (action[0].equals("lookaround")) {
				System.out.printf(
					"%s is searching for something...\n", 
					player.getName()
				);
				// random chance of getting new item
				double randOut = rand.nextDouble();
				if (randOut < 0.6) {
					System.out.printf("%s found some items!\n", 
					player.getName());
					player.putItem(
						itemServer.get(rand.nextInt(itemServer.size()))
					);
					player.gainExp(randOut * 10);
				}
				else {
					System.out.printf(
						"%s does not found anything interesting.\n", 
						player.getName()
					);
				}
			}
			else if (action[0].equals("!spawn")) {
				// hey we have a dev console command now
				if (action.length == 2) {
					for (Item spawn : itemServer) {
						if (spawn.getName().toLowerCase().contains(action[1])) {
							player.putItem(spawn);
							break;
						}
					}
				}
				
			}
			else if (action[0].equals("list")) {
				if (action.length == 2) {
					if (action[1].matches("(inv|bag)")) {
						player.listItems();
					}
					else if (action[1].equals("equip")) {
						player.listEquipments();
					}
				}
				else {
					//list all
					player.listItems();
					player.listEquipments();
				}
			}
			else if (action[0].matches("stat(us)?")) {
				if (scene == null) {
					player.statusCheck();
				}
				else {
					System.out.println(" **** Attacker **** ");
					scene.getAttacker().statusCheck();
					System.out.println(" **** Attacked **** ");
					scene.getAttacked().statusCheck();
				}
				
			}
			else if (action[0].equals("battle")) {
				Student target = null;
				if (action.length == 2) {
					// search for a student
					for (Student std : students) {
						if (std.getName().toLowerCase().contains(action[1])) {
							target = std;
							break;
						}
					}

					if (target != null && target != player)  {
						if (scene == null) {
							scene = new NewBattleAction(player, target);
							System.out.printf(
								"Initiated a battle with '%s'\n",
								target.getName()
							);
						}
						else {
							System.out.println("Already in a battle.");
						}
					}
					else if (target == player) {
						System.out.println("You can't battle with yourself.");
					}
					else {
						System.out.printf(
							"E: there's no one called '%s' to battle\n", 
							action[1]
						);
					}

				}
				else {
					System.out.println("E: No target specified.");
				}
			} 
			else if (action[0].contains("attack")) {
				if (scene != null) {
					scene.resolveAction();
					if (!scene.isCombatantsAlive()) {
						System.out.printf(
							"Battle finished, '%s' survives.\n",
							scene.getSurvivor().getName()
						);
						scene = null;
					}
				}
				else {
					System.out.println("Not in a battle");
				}
			}
			else if (action[0].contains("flee")) {
				if (scene != null) {
					System.out.printf("'%s' withdrawn from battle with '%s'.\n",
						player.getName(),
						scene.getOpponent(player).getName()
					);
					scene = null;
				}
			}
			else if (action[0].equals("hand")) {
				if (player.holding() != null) {
					player.holding().print();
				}
				else {
					System.out.printf("%s hand is empty.\n", player.getName());
				}
			}
			else if (action[0].equals("store")) {
				if (player.holding() != null) {
					player.putItem(player.holding());
					player.discardHand();
				}
				else {
					System.out.printf("%s hand is empty.\n", player.getName());
				}
			}
			else if (action[0].equals("pull")) {
				if (action.length == 2) {
					player.pullItem(action[1]);
				}
				else {
					System.out.println("E: no item specified");
				}
			}
			else if (action[0].equals("search")) {
				if (action.length == 2) {
					player.searchItem(action[1]);
				}
				else {
					System.out.println("E: no item specified");
				}
			}
			else if (action[0].equals("use")) {
				if (player.holding() != null) {
					if (! player.useItem(player.holding())) {
						System.out.printf(
							"%s refused to use '%s' on hand.\n",
							player.getName(), player.holding().getName());
					}
					else {
						player.discardHand();
					}
				}
				else {
					System.out.printf("%s hand is empty.\n", player.getName());
				}
			}
			else if (action[0].equals("equip")) {
				if (action.length == 2) {
					player.useItemFromBag(action[1]);
				}
				else {
					System.out.println("E: no item specified");
				}
			}
			else if (action[0].equals("unequip")) {
				if (action.length == 2) {
					Item ueq = null;
					if (action[1].equals("apparel")) {
						ueq = player.unequipApparel();
						if (ueq != null) {
							System.out.printf("%s unequipped '%s'\n", 
								player.getName(), ueq.getName());
						}
						else {
							System.out.printf("%s apparel unequip failed.\n",
								player.getName());
						}
					}
					else if (action[1].equals("gear")) {
						ueq = player.unequipGear();
						if (ueq != null) {
							System.out.printf("%s unequipped '%s'\n", 
								player.getName(), ueq.getName());
						}
						else {
							System.out.printf("%s gear unequip failed.\n",
								player.getName());
						}
					}
				}
				else {
					System.out.println("E: unequip apparel or gear?");
				}
			}
			else if (action[0].equals("discard")) {
				Item held = player.holding();
				if (player.discardHand()) {
					System.out.printf("%s discarded '%s' from their hand.\n", 
						player.getName(), held.getName());
				}
				else {
					System.out.printf("%s hand is empty.\n", 
						player.getName());
				}

			}
			else if (action[0].equals("help")) {
				System.out.println("Avaliable Commands:");
				System.out.println("> switch <name> \t-- assume control of different character");
				System.out.println("> stat[us] \t\t-- check status");
				System.out.println("> walk <steps> \t\t-- take a walk, chance to find an item");
				System.out.println("> lookaround \t\t-- look around, might find an item");
				System.out.println("> battle <student> \t-- initiate battle with a student");
				System.out.println("> list [inv|bag|equip] \t-- list inventory or current equipment");
				System.out.println("> search <item name> \t-- look for item in their bag");
				System.out.println("> pull <item name> \t-- pull an item from their bag");
				System.out.println("> hand \t\t\t-- list item on hand");
				System.out.println("> store \t\t-- store item on hand to the bag");
				System.out.println("> use \t\t\t-- use an item on hand, either consume or equip");
				System.out.println("> equip <item name> \t-- shortcut for pull+use usable item");
				System.out.println("> unequip apparel|gear \t-- unequip specific slot");
				System.out.println("> discard \t\t-- dscard item on hand");
				System.out.println("> quit|exit \t\t-- exit the tester");
			}
			else if (action[0].matches("(quit|exit)")) {
				System.out.println("Goodbye.");
				break;
			}
			else {
				System.out.printf("E: malformed input: '%s'\n", input);
				System.out.println("   try again or check 'help' command.");
			}
		} while (!(
			input.contains("quit") || input.contains("exit")
		));

		scanner.close();
	}

	private static double expFromWalking(int steps) {
		return steps * 0.8;
	}
}