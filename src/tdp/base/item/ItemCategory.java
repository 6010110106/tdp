package tdp.base.item;

public enum ItemCategory {
	REGULAR, 
	GEAR,
	APPAREL,
	CONSUMABLE,
	MATERIAL
}