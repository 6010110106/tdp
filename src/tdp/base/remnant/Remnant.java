package tdp.base.remnant;

import java.util.*;
import tdp.base.item.*;
import tdp.base.combat.*;
import tdp.base.student.*;

public class Remnant extends Student {
	private String style = "Remnant";
	private StudentAttributes remnantBonus;

	public Remnant(String name, int initHealth, int initFocus, int maxSlots) {
		super(name, initHealth, initFocus, maxSlots);
		super.style = this.style;
	}

	public Remnant(String name, int initHealth, int initFocus) {
		this(name, initHealth, initFocus, 5);
	}

	public Remnant(Student grad) {
		super(grad);
		super.style = this.style;
	}

	// defensive clone
	@Override
	public Remnant recreate() {
		return new Remnant(this);
	}
}