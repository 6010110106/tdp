package tdp.base.combat;

import tdp.base.student.*;

public interface Combatant {
	public boolean performAttack(Student target);
	public boolean resolveAttack(int oroll, Student attacker);
	public int getDamageModifier();
	public int getDefenseModifier();
}