package tdp.base.combat;

import tdp.base.item.*;
import tdp.base.student.*;
import java.util.*;

public class BattleAction {
	private Student leftStudent;
	private Student rightStudent;
	StudentAttributes leftAttrib;
	StudentAttributes rightAttrib;

	public BattleAction(Student left, Student right) {
		this.leftStudent = left;
		this.rightStudent = right;

		this.leftAttrib = this.leftStudent.getAttribs();
		this.rightAttrib = this.rightStudent.getAttribs();
	}

	public Student getLeftStudent() { return this.leftStudent; }
	public Student getRightStudent() { return this.rightStudent; }

	public boolean resolveBattle() {
		if (!this.leftStudent.isAlive() || !this.rightStudent.isAlive()) {
			// one side is dead
			return false;
		}
		if (this.leftAttrib.getSpeed() > this.rightAttrib.getSpeed()) {
			this.leftAttack();
			this.rightAttack();
		}
		else if (this.leftAttrib.getSpeed() < this.rightAttrib.getSpeed()) {
			this.rightAttack();
			this.leftAttack();
		}
		else {
			Random rng = new Random();
			if (rng.nextDouble() < 0.5) {
				this.leftAttack();
				this.rightAttack();
			}
			else {
				this.rightAttack();
				this.leftAttack();
			}
		}
		return true;
	}

	public boolean isCombatantsAlive() {
		return (this.leftStudent.isAlive() && this.rightStudent.isAlive());
	}

	public Student getSurvivor() {
		if (!this.isCombatantsAlive()) {
			if (this.leftStudent.isAlive()) {
				return this.leftStudent;
			}
			else {
				return this.rightStudent;
			}
		}
		return null;
	}

	public Student getOpponent(Student attacker) {
		if (attacker == this.leftStudent) {
			return this.rightStudent;
		}
		else if (attacker == this.rightStudent) {
			return this.leftStudent;
		}
		else {
			return null;
		}
	}

	private void leftAttack() {
		int dmg = this.calcDamage(this.leftStudent, this.rightStudent);
		this.rightStudent.takeDamage(dmg);
	}

	private void rightAttack() {
		int dmg = this.calcDamage(this.rightStudent, this.leftStudent);
		this.leftStudent.takeDamage(dmg);
	}

	public int calcDamage(Student attacker, Student receiver) {
		Random d6 = new Random();
		int roll = d6.nextInt(5) + 1;
		roll += d6.nextInt(5) + 1;
			// fake 2d6 roll
		int damage = roll;
		int atkMod = attacker.getDamageModifier();
		int defMod = receiver.getDefenseModifier();

		damage = roll + atkMod - defMod;
		if (damage < 0) {
			damage = 0;
		}

		return damage;
	}
}