package tdp.base.student;

public class StudentAttributes {
	private int speed;
	private int strength;
	private int intelligence;
	private int luck;

	public StudentAttributes() {
		this.speed = 3;
		this.strength = 3;
		this.intelligence = 3;
		this.luck = 3;
	}

	public StudentAttributes(int sp, int st, int in, int lk) {
		this.speed = sp;
		this.strength = st;
		this.intelligence = in;
		this.luck = lk;
	}

	public int getStrength() { return this.strength; }
	public int getIntel() { return this.intelligence; }
	public int getSpeed() { return this.speed; }
	public int getLuck() { return this.luck; }

	public void addStr(int amt) {
		if (amt > 0) {
			this.strength += amt;
		}
	}
	
	public void addInt(int amt) {
		if (amt > 0) {
			this.intelligence += amt;
		}
	}

	public void addSpeed(int amt) {
		if (amt > 0) {
			this.speed += amt;
		}
	}

	public void addLuck(int amt) {
		if (amt > 0) {
			this.luck += amt;
		}
	}

	public void addStats(int sp, int st, int in, int lk) {
		this.addSpeed(sp);
		this.addStr(st);
		this.addInt(in);
		this.addLuck(lk);
	}

	public void addDelta(StudentAttributes delta) {
		this.addSpeed(delta.speed);
		this.addStr(delta.strength);
		this.addInt(delta.intelligence);
		this.addLuck(delta.luck);
	}
}