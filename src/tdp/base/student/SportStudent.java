package tdp.base.student;

import java.util.*;
import tdp.base.combat.*;
import tdp.base.student.*;

public class SportStudent extends Student implements Skilled, Combatant {
	private String style = "Sport";
	private int tired;
	private StudentAttributes sportBonus;

	public SportStudent(
		String name, int initHealth, int initFocus, int maxSlots) {
		super(name, initHealth, initFocus, maxSlots);
		super.style = this.style;
		this.sportBonus = new StudentAttributes(1, 2, 0, 0);
		super.setBonusAttrib(this.sportBonus);
		super.setLevelUpStep(7, 4);

		this.tired = 0;
	}

	public SportStudent(String name, int initHealth, int initFocus) {
		this(name, initHealth, initFocus, 5);
	}

	public SportStudent(Student grad) {
		super(grad);
		super.style = this.style;
		this.sportBonus = new StudentAttributes(1, 2, 0, 0);
		super.setBonusAttrib(this.sportBonus);
		super.setLevelUpStep(7, 4);

		this.tired = 0;
	}

	@Override
	public SportStudent recreate() {
		return new SportStudent(this);
	}

	@Override
	public void primarySkill(Student target) {
		// direct attack with twice str modifier but without weapon mod
		// cannot be used twice in a row
		if (super.consumeFocus(3) == 3 && this.alive) {
			Random rng = new Random();
			int roll = rng.nextInt(10)+2;
				// fake 2d6
			if (this.tired < 1) {
				target.resolveDirectAttack(roll+this.attrib.getStrength()*2, this);
				this.tired += 1;
			}
		}
		// TODO: else errors...
	}

	@Override
	public void secondarySkill(Student target) {
		// attempt to attack twice in succession
		// even more tiring
		if (super.consumeFocus(7) == 7 && this.alive) {
			target.resolveAttack(0, this);
			target.resolveAttack(0, this);
			this.tired += 2;
		}
	}

	@Override
	public void auxillarySkill(Student target) {
		this.rest();
	}

	public boolean performAttack(Student target) {
		boolean result = target.resolveAttack(0, this);
		this.rest();
		return result;
	}
	public boolean resolveAttack(int oroll, Student attacker) {
		boolean result = super.resolveAttack(oroll, attacker);
		this.rest();
		return result;
	}

	private void rest() {
		if (this.tired > 0) {
			this.tired -= 1;
		}
	}
}