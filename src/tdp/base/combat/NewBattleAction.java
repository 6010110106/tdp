package tdp.base.combat;

import java.util.*;
import tdp.base.student.*;
import tdp.base.item.*;

public class NewBattleAction {
	private Student attacker;
	private Student attacked;

	private StudentAttributes attackerAttrib;
	private StudentAttributes attackedAttrib;

	private String attackAction;
		// possible value: "attack", "primary", "secondary", "aux",
		// "item:<item name>"
	private Random rng;
	private boolean attackerIdle;

	/**
	 * new BattleAction (think of it as one battle transaction)
	 * @param attacker a student who does the attack
	 * @param attacked target student of attacker
	 * @param act action type, one of "attack", "item:itemName", "flee"
	 */
	public NewBattleAction(Student attacker, Student attacked, String act) {
		this.attacker = attacker;
		this.attacked = attacked;

		this.attackerIdle = false;

		if (attacked != null) {
			this.attackerAttrib = attacker.getAttribs();
			this.attackedAttrib = attacked.getAttribs();
		}
		else {
			this.attackerIdle = true;
		}

		

		this.attackAction = act;

		this.rng = new Random();
	}

	public NewBattleAction(Student attacker, Student attacked) {
		this(attacker, attacked, "attack");
	}

	public Student getAttacker() { return this.attacker; }
	public Student getAttacked() { return this.attacked; }
	public String getActionType() { return this.attackAction; }

	public boolean isCombatantsAlive() {
		if (this.attackerIdle) {
			return this.attacker.isAlive();
		}
		return (this.attacker.isAlive() && this.attacked.isAlive());
	}

	public boolean resolveAction() {
		boolean actSuccess = false;
		if (!this.attackerIdle) {
			int speedDiff = attackerAttrib.getSpeed()
				- attackedAttrib.getSpeed();
			if (this.attackAction.equals("attack")) {
				if (speedDiff > 0) {
					return this.resolveRegularAttack();
				}
				else {
					if (rng.nextDouble() < (1 + (speedDiff * 0.025))) {
						return this.resolveRegularAttack();
					}
					else {
						// attacked missed
						return false;
					}
				}
			}
			else if (this.attackAction.equals("primary")) {
				return this.resolvePrimarySkill();
			}
			else if (this.attackAction.equals("secondary")) {
				return this.resolveSecondarySkill();
			}
			else if (this.attackAction.startsWith("item:")) {
				String itemName[] = this.attackAction.split(":", 2);
				return this.resolveItemUse(itemName[1]);
			}
			else if (this.attackAction.equals("flee")) {
				return resolveBattleEscape();
			}
		}
		return actSuccess;
	}

	public Student getSurvivor() {
		if (!this.isCombatantsAlive()) {
			if (this.attacker.isAlive()) {
				return this.attacker;
			}
			else {
				return this.attacked;
			}
		}
		return null;
	}

	public Student getOpponent(Student attacker) {
		if (attacker == this.attacker) {
			return this.attacked;
		}
		else if (attacker == this.attacked) {
			return this.attacker;
		}
		else {
			return null;
		}
	}

	private boolean resolveItemUse(String itemName) {
		if (this.attacker.searchItem(itemName)) {
			return this.attacker.useItemFromBag(itemName);
		}
		else {
			// item not exist in their bag
			return false;
		}
	}

	private boolean resolveRegularAttack() {
		return this.attacked.resolveAttack(
			this.rollDices(6, 2),
			this.attacker
		);
	}

	private boolean resolvePrimarySkill() {
		if (this.attacker instanceof Skilled) {
			Skilled atker = (Skilled) this.attacker;
			atker.primarySkill(this.attacked);
			return true;
		}
		else {
			return false;
		}
	}

	private boolean resolveSecondarySkill() {
		if (this.attacker instanceof Skilled) {
			Skilled atker = (Skilled) this.attacker;
			atker.secondarySkill(this.attacked);
			return true;
		}
		else {
			return false;
		}
	}

	private boolean resolveBattleEscape() {
		boolean escaped = false;
		int luckDiff = this.attackerAttrib.getLuck() 
			- this.attackedAttrib.getLuck();
		if (luckDiff > 0) {
			if (rng.nextDouble() < (0.7 + luckDiff*0.025)) {
				escaped = true;
			}
		}
		else {
			escaped = (rng.nextDouble() < 0.25);
		}
		return escaped;
	}

	private int rollDices(int sides, int dices) {
		int roll = 0;
		if (dices < 1) {
			return 0;
		}
		for (int i=0; i<dices; i++) {
			roll += rng.nextInt(sides)+1;
		}
		return roll;
	}
}
