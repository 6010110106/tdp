package tdp.ui;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.*;

import tdp.base.student.*;
import tdp.base.remnant.*;
import tdp.base.item.*;

public class StudentInfoPanel extends JPanel {
	private JPanel barPanel;
	private JPanel displayPanel;
	private JPanel extraInfoPanel;
	private JPanel attribPanel;
	private JPanel sidePanel;

	private TitledBorder infoBorder;
	private TitledBorder extraInfoBorder;
	private TitledBorder attribBorder;

	private JProgressBar influenceBar;
	private JProgressBar focusBar;
	private JProgressBar expBar;

	private JLabel nameLabel;
	private JLabel iconLabel;

	private JLabel currentGear;
	private JLabel currentApparel;
	private JLabel killCount;

	private JTextArea attribText;

	private BoxLayout barPanelLayout;
	private LayoutManager panelLayout;

	private GridBagConstraints attribConst;

	private Student student;

	public StudentInfoPanel(Student student) {
		this();
		this.setStudent(student);
	}

	public StudentInfoPanel() {
		//this.panel = new JPanel();
		super();
		this.panelLayout = new BorderLayout();
		this.setLayout(this.panelLayout);
		
		this.barPanel = new JPanel();
		this.barPanelLayout = new BoxLayout(this.barPanel, BoxLayout.Y_AXIS);
		this.barPanel.setLayout(this.barPanelLayout);

		this.setOpaque(false);

		this.influenceBar = new JProgressBar(0, 100);
		this.influenceBar.setStringPainted(true);
		this.focusBar = new JProgressBar(0, 100);
		this.focusBar.setStringPainted(true);
		this.expBar = new JProgressBar(0, 100);
		this.expBar.setStringPainted(true);

		this.nameLabel = new JLabel("Nada...");
		this.iconLabel = new JLabel();

		this.extraInfoPanel = new JPanel();
		this.extraInfoPanel.setLayout(
			new BoxLayout(this.extraInfoPanel, BoxLayout.Y_AXIS)
		);
		this.extraInfoBorder = new TitledBorder("Info");
		this.extraInfoPanel.setBorder(this.extraInfoBorder);

		this.currentApparel = new JLabel("Apparel: None");
		this.currentApparel.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		this.currentGear = new JLabel("Gear: None");
		this.currentGear.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		this.killCount = new JLabel("Kills: None");
		this.killCount.setVisible(false);
		this.killCount.setAlignmentX(JLabel.LEFT_ALIGNMENT);

		this.attribPanel = new JPanel();
		this.attribPanel.setLayout(new GridBagLayout());
		this.attribBorder = new TitledBorder("Points");
		this.attribPanel.setBorder(this.attribBorder);

		this.attribText = new JTextArea();
		this.attribText.setFont(new Font(null, Font.PLAIN, 12));
		this.attribText.setEditable(false);
		this.attribPanel.add(this.attribText);

		this.attribConst = new GridBagConstraints();
		this.attribConst.weightx = 1;
		this.attribConst.fill = GridBagConstraints.HORIZONTAL;
		this.attribConst.gridwidth = GridBagConstraints.REMAINDER;

		this.extraInfoPanel.add(Box.createVerticalGlue());
		this.extraInfoPanel.add(this.currentApparel);
		this.extraInfoPanel.add(Box.createVerticalStrut(4));
		this.extraInfoPanel.add(this.currentGear);
		this.extraInfoPanel.add(Box.createVerticalStrut(4));
		this.extraInfoPanel.add(this.killCount);
		this.extraInfoPanel.add(Box.createVerticalGlue());
		
		this.barPanel.add(Box.createHorizontalGlue());
		this.barPanel.add(this.influenceBar);
		this.barPanel.add(this.focusBar);
		this.barPanel.add(Box.createVerticalStrut(4));
		this.barPanel.add(this.expBar);
		this.barPanel.add(Box.createHorizontalGlue());

		this.displayPanel = new JPanel(new FlowLayout());
		this.displayPanel.add(this.iconLabel);
		this.displayPanel.add(this.barPanel);

		
		this.barPanel.setAlignmentX(Component.RIGHT_ALIGNMENT);
		this.nameLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		this.displayPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

		this.infoBorder = new TitledBorder("Nada...");
		this.displayPanel.setBorder(this.infoBorder);
		
		this.add(this.displayPanel, BorderLayout.WEST);
		this.add(this.extraInfoPanel, BorderLayout.CENTER);
		this.add(this.attribPanel, BorderLayout.EAST);
		this.setAlignmentX(JPanel.LEFT_ALIGNMENT);
	}

	public Student getTargetStudent() { return this.student; }

	public void setStudent(Student student) {
		this.student = student;
		if (student.hasIcon()) {
			this.loadStudentIcon();
		}
		this.updateStudentInfo();
	}

	public boolean loadStudentIcon() {
		String iconRes = this.student.getIconResource();
		java.net.URL imgURL = getClass().getResource(iconRes);
		BufferedImage studentIcon = null;
		if (imgURL != null) {
			try {
				studentIcon = ImageIO.read(imgURL);
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}

			if (studentIcon != null) {
				this.iconLabel.setIcon(new ImageIcon(studentIcon));
				return true;
			}
		}
		
		return false;
	}

	public String formatStudentAttrib() {
		String out = "";
		out = String.format(
			"Str: %d\nSpeed: %d\nInt: %d\nLuck: %d",
			this.student.getAttribs().getStrength(), 
			this.student.getAttribs().getSpeed(), 
			this.student.getAttribs().getIntel(), 
			this.student.getAttribs().getLuck()
		);
		return out;
	}

	public void updateStudentInfo() {
		this.nameLabel.setText(this.student.getName());
		this.infoBorder.setTitle(String.format(
			"%s (%s)",
			this.student.getName(),
			this.student.getStyle()
		));

		this.influenceBar.setMaximum(this.student.getMaxHealth());
		this.influenceBar.setValue(this.student.getHealth());
		this.influenceBar.setString(
			String.format(
				"Influence: %d / %d",
				this.student.getHealth(),
				this.student.getMaxHealth()
			)
		);

		this.focusBar.setMaximum(this.student.getMaxFocus());
		this.focusBar.setValue(this.student.getFocus());
		this.focusBar.setString(
			String.format(
				"Focus: %d / %d",
				this.student.getFocus(),
				this.student.getMaxFocus()
			)
		);

		this.expBar.setMaximum(this.student.getExpCap()*10);
		Float expDisp = this.student.getExp() * 10;
		this.expBar.setValue(expDisp.intValue());
		this.expBar.setString(
			String.format(
				"L%d: %.2f / %d",
				this.student.getLevel(),
				this.student.getExp(),
				this.student.getExpCap()
			)
		);
		this.updateEquippedItems(this.student.currentApparel(), "Apparel");
		this.updateEquippedItems(this.student.currentGear(), "Gear");
		if (!(this.student instanceof Remnant) ) {
			this.killCount.setVisible(true);
		}
		this.killCount.setText(
			String.format(
				"Kills: %d",
				this.student.getKills()
			)
		);

		this.attribText.setText(this.formatStudentAttrib());
	}

	private void updateEquippedItems(Item pending, String itype) {
		JLabel labelDisplay = null;

		if (itype.toLowerCase().equals("apparel")) {
			labelDisplay = this.currentApparel;
		}
		else if (itype.toLowerCase().equals("gear")) {
			labelDisplay = this.currentGear;
		}

		if (pending != null && labelDisplay != null) {
			labelDisplay.setText(
				String.format("%s: %s", itype, pending.getName())
			);
		}
		else {
			labelDisplay.setText(
				String.format("%s: %s", itype, "None")
			);
		}
	}
}