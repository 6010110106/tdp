package tdp.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import java.util.*;

import tdp.base.combat.*;
import tdp.base.student.*;
import tdp.base.remnant.*;
import tdp.base.item.*;

public class BattlePanel extends JPanel {
	ArrayList<NewBattleAction> pendingAction;

	StudentInfoPanel studentInfo;
	StudentInfoPanel remnantInfo;

	Student student;
	Remnant remnant;
	RemnantMastermind mastermind;

	JPanel studentAction;

	TitledBorder actionBorder;

	JButton attackButton;
	JButton skillButton;
	JButton fleeButton;

	public BattlePanel() {
		super();
		super.setLayout(new BorderLayout());

		this.pendingAction = new ArrayList<>();

		this.student = null;
		this.remnant = null;
		this.mastermind = new RemnantMastermind();

		this.studentInfo = new StudentInfoPanel();
		this.remnantInfo = new StudentInfoPanel();

		this.studentAction = new JPanel();
		this.studentAction.setLayout(
			new BoxLayout(this.studentAction, BoxLayout.Y_AXIS)
		);
		this.actionBorder = new TitledBorder("Actions");
		this.studentAction.setBorder(this.actionBorder);

		this.attackButton = new JButton("Attack!");
		this.attackButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				BattlePanel self = BattlePanel.this;
				NewBattleAction pending = new NewBattleAction(
					self.student, self.remnant, "attack"
				);
				self.addPendingActs(pending);

				// for simplicity's sake, i'll just make the remnant
				// randomly queue for attack or idle, for the time being
				self.addPendingActs(
					mastermind.commandRemnant(self.remnant, self.student)
				);

				self.resolveActions();
			}
		});

		this.skillButton = new JButton("Skill");

		this.fleeButton = new JButton("Escape");
		this.fleeButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				BattlePanel self = BattlePanel.this;
				NewBattleAction pending = new NewBattleAction(
					self.student, self.remnant, "flee"
				);
				self.addPendingActs(pending);

				self.addPendingActs(
					mastermind.commandRemnant(self.remnant, self.student)
				);

				self.resolveActions();
			}
		});

		this.studentAction.add(this.attackButton);
		this.studentAction.add(Box.createGlue());
		this.studentAction.add(this.fleeButton);

		this.add(this.remnantInfo, BorderLayout.WEST);
		this.add(this.studentInfo, BorderLayout.EAST);
		this.add(this.studentAction, BorderLayout.CENTER);
	}

	public void updateCombatantStatus() {
		this.remnantInfo.updateStudentInfo();
		this.studentInfo.updateStudentInfo();
	}

	public boolean resolveActions() {
		boolean deaths = false;
		boolean finished = false;
		for (NewBattleAction act : this.pendingAction) {
			String actType = act.getActionType();
			boolean actSuccess = act.resolveAction();
			this.updateCombatantStatus();
			//System.out.printf("[%b] %s\n", actSuccess, actType);
			if (actType.equals("flee") && actSuccess) {
				JOptionPane.showMessageDialog(
					null, 
					act.getAttacker() + " escaped from the battle."
				);
				finished = true;
				break;
			}

			if (!act.isCombatantsAlive()) {
				JOptionPane.showMessageDialog(
					null, 
					act.getSurvivor() + " won the battle."
				);
				deaths = true;
				finished = true;
				break;
			}
		}

		if (finished) {
			this.battleEndCleanup();
		}

		this.resetPendingActs();
		return deaths;
	}

	public void battleEndCleanup() {
		this.attackButton.setEnabled(false);
		this.fleeButton.setEnabled(false);
		JFrame parent = (JFrame)SwingUtilities.getWindowAncestor(this);
		parent.dispose();
	}

	public boolean isCombatantsAlive() {
		return (this.remnant.isAlive() && this.student.isAlive());
	}

	public void addPendingActs(NewBattleAction act) {
		this.pendingAction.add(act);
	}

	public void resetPendingActs() {
		this.pendingAction.clear();
	}

	public void setCombatants(Remnant remnant, Student student) {
		this.remnant = remnant;
		this.remnantInfo.setStudent(remnant);

		this.student = student;
		this.studentInfo.setStudent(student);

		this.attackButton.setEnabled(true);
		this.fleeButton.setEnabled(true);
	}

	public static void main(String[] args) {
		RemnantMastermind mastermind = new RemnantMastermind();
		SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
				JFrame frame = new JFrame();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				BattlePanel battle = new BattlePanel();

				Student saihara = new IntellectStudent(
					"Saihara Shuichi", 25, 40
				);

				Remnant remnant = mastermind.birthRemnant(saihara);

				battle.setCombatants(remnant, saihara);

				frame.getContentPane().add(battle);
				frame.pack();
				frame.setVisible(true);
			}
        });
	}
}
