package tdp.base.combat;

import java.util.*;
import tdp.base.student.*;

public class BattleParty {
	// key stores party position [left, center, right]
	private HashMap<String, Student> party;
	private String possiblePositions[] = {"left", "center", "right"};

	public BattleParty() {
		this.party = new HashMap<>();
	}

	public HashMap<String, Student> getPartyMap() { return this.party; }

	public void setPartyPos(String pos, Student student) {
		for (String ppos : this.possiblePositions) {
			if (ppos.contains(pos)) {
				party.putIfAbsent(pos, student);
				break;
			}
		}
	}



}