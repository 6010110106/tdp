package tdp.base.item;

import java.util.*;

import tdp.base.student.*;

public class ConsumableItem extends Item {
	private int recoverPoint;
	private ConsumeType recover;

	//private String consumableType = "Influence";
		// planned type: [Influence, Focus, Both]

	public ConsumableItem(String itemName, String flavor, Integer recovery, 
		ConsumeType restores) {
		super(itemName, ItemCategory.CONSUMABLE, flavor);
		this.recover = restores;
		this.recoverPoint = recovery;
	}

	public ConsumableItem(String itemName, String flavor, Integer recovery) {
		this(itemName, flavor, recovery, ConsumeType.INFLUENCE);
	}

	public ConsumableItem(String itemName, Integer recovery) {
		this(itemName, "An ordinary " + itemName, recovery);
	}

	public ConsumeType getRecoveryType() { return this.recover; }
	public int getRecoveryPoint() { return this.recoverPoint; }

	public String describe() {
		String lines = super.describe();
		String recoverDisp = recover.name().toLowerCase();

		if (recoverDisp.equals("both")) {
			recoverDisp = "influence AND focus";
		}
		lines += String.format("Uses:\t recovers %d %s.\n", 
			this.recoverPoint, recoverDisp);
		
		return lines;
	}
	
	public Integer use() {
		return this.recoverPoint;
	}

	/*
	public boolean resolveUse(Student patient) {
		// not yet implemented, can't wrap my head around this yet
		// implement func to applied correct type of restore to given student
		return false;
	}
	*/
}