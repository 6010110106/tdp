package tdp.base.item;

public class Item {
	// item attrib
	private String itemName;
	private ItemCategory itemType;
	private String flavorText;
	
	// Constructor full
	public Item(String itemName, ItemCategory itemType, String flavor) {
		this.itemName = itemName;
		this.itemType = itemType;
		this.flavorText = flavor;
	}
	
	// Constructor overload: name only
	public Item(String itemName) {
		this(itemName, ItemCategory.REGULAR, "A normal-looking " + itemName);
	}

	// Constructor overload: name and type
	public Item(String itemName, ItemCategory itemType) {
		this(itemName, itemType, "A normal-looking " + itemName);
	}

	// Constructor overload: name and flavor
	public Item(String itemName, String flavor) {
		this(itemName, ItemCategory.REGULAR, flavor);
	}

	// bunch of getters
	public String getName() { return this.itemName; }
	public String getFlavorText() { return this.flavorText; }
	public ItemCategory getCatagory() {	return this.itemType; }

	public String toString() {
		return this.itemName;
	}

	public boolean isSameAs(Item item) {
		boolean sameItem;
		sameItem = (this.itemName.equals(item.getName()));
		sameItem = sameItem && (this.itemType == item.getCatagory());
		sameItem = sameItem && (this.flavorText.equals(item.getFlavorText()));
		return sameItem;
	}
	public void print() {
		System.out.print(this.describe());
	}

	/**
	 * return general description of this item
	 * @return (formatted) string represent item properties
	 */
	public String describe() {
		String lines = "";
		lines = String.format("Type:\t %s\n", itemType.name().toLowerCase());
		lines += String.format("Item:\t %s\n", this.itemName);
		lines += String.format("Desc:\t %s\n", this.flavorText);

		return lines;
	}

	public Integer use() {
		// placeholder of using item function, defaults to inspect them
		// planned for it to returns item specific values
		this.describe();
		return 0;
	}
} 