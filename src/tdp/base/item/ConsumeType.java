package tdp.base.item;

public enum ConsumeType {
	INFLUENCE,
	FOCUS,
	BOTH
}
