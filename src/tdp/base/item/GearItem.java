package tdp.base.item;

public class GearItem extends Item {
	// attackPower as a dmg modifier, probably dnd-like system is planned
	private Integer attackPower;

	public GearItem(
		String itemName, String flavor, Integer atkPower) {
		super(itemName, ItemCategory.GEAR, flavor);
		this.attackPower = atkPower;
	}

	public GearItem(String itemName, Integer atkPower) {
		this(itemName, "Standard issue "+itemName, atkPower);
	}

	public Integer getAttackPower() { return this.attackPower; }

	public String describe() {
		String lines = super.describe();
		lines += String.format(
			"Uses:\t equipable, +%d to damage\n",
			this.attackPower
		);

		return lines;
	}

	/**	
	 * GearItem: enhance base damage of user by whatever percent
	 * 
	 * @return base damage multiplier percent (as int) so 100 means 1x damage
	 * multiplier
	 */
	public Integer use() {
		return this.attackPower;
	}
}