package tdp.base.student;

import java.util.*;
import tdp.base.item.*;
import tdp.base.combat.*;

public class IntellectStudent extends Student implements Skilled {
	private String style = "Intellect";
	private StudentAttributes intellectBonus;

	public IntellectStudent(
		String name, int initHealth, int initFocus, int maxSlots) {
		super(name, initHealth, initFocus, maxSlots);
		super.style = this.style;
		this.intellectBonus = new StudentAttributes(0, 0, 3, 0);
		super.setBonusAttrib(this.intellectBonus);
		super.setLevelUpStep(4, 10);
			// intellect student gets more focus per level up than others
	}

	public IntellectStudent(String name, int initHealth, int initFocus) {
		this(name, initHealth, initFocus, 5);
	}

	public IntellectStudent(Student grad) {
		super(grad);
		super.style = this.style;
		this.intellectBonus = new StudentAttributes(0, 0, 3, 0);
		super.setBonusAttrib(this.intellectBonus);
		super.setLevelUpStep(4, 10);
	}

	// defensive clone
	@Override
	public IntellectStudent recreate() {
		return new IntellectStudent(this);
	}

	public void primarySkill(Student target) {

	}

	public void secondarySkill(Student target) {

	}

	public void auxillarySkill(Student target) {
		// absorb enemy focus
		int pending = this.level*2 + this.attrib.getIntel();
		this.restoreFocus(target.consumeFocus(pending));
	}
}