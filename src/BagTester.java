
import tdp.base.student.*;
import tdp.base.item.*;

public class BagTester {
	public static void main(String[] args) {
		Student hinata = new Student("Hinata Hajime", 30, 24, 10);
		Student nanami = new Student("Nanami Chiaki", 22, 31);
		IntellectStudent kamukura;

		// basic character attrib add/del
		hinata.takeDamage(22);
		nanami.gainExp(32.0);

		// list status: empty
		hinata.statusCheck();
		nanami.statusCheck();

		// register some items
		Item necktie = new Item("Necktie", ItemCategory.APPAREL);
		Item hairclip = new Item("Almost-Galaga Hairclip", 
			ItemCategory.APPAREL);
		Item gacha = new Item("Mysterious Capsule");

		// put some in the bags
		hinata.putItem(necktie);

		nanami.putItem(hairclip);
		nanami.putItem(gacha);
		nanami.putItem(gacha);
		nanami.putItem(gacha);
		nanami.putItem(gacha);
		nanami.putItem(necktie);

		// list items again
		hinata.listItems();
		nanami.listItems();

		// look for some items
		hinata.searchItem(necktie.getName());
		nanami.searchItem(gacha.getName());

		// tries to pull out items, overpull too
		nanami.pullItem(gacha.getName());
		nanami.pullItem(gacha.getName());
		nanami.pullItem(gacha.getName());
		nanami.pullItem(gacha.getName());
		nanami.pullItem(gacha.getName());

		// list status: modded
		hinata.statusCheck();
		nanami.statusCheck();

		/*
		 * FIXME: actually graduate student, not SOMA level copy&ignore old self shit like this
		 * 'graduate' test
		 *     the idea is instead of storing novice in itself and change 
		 *     stored subclass at 'job change', have the world 'graduate' 
		 *     it into another subclass instead.
		 *     the base student who graduated should be scrapped after this
		 *     i guess?
		 */
		System.out.println(" **** Here marks the point of graduation **** ");
		kamukura = new IntellectStudent(hinata);
		kamukura.statusCheck();
		hinata.statusCheck();

		kamukura.listItems();
		hinata.listItems();

		kamukura.getStyle();
		hinata.getStyle();

		nanami.listItems();
		kamukura.listItems();
		hinata.listItems();
	}
}