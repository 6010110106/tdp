package tdp.base.item;

public class ApparelItem extends Item {
	private Integer defPoint;

	public ApparelItem(String itemName, String flavor, Integer defPoint) {
		super(itemName, ItemCategory.APPAREL, flavor);
		this.defPoint = defPoint;
	}

	public ApparelItem(String itemName, Integer defPoint) {
		this(itemName, "A standard " + itemName, defPoint);
	}

	public Integer getDefPoint() { return this.defPoint; }

	public String describe() {
		String lines = super.describe();
		lines += String.format(
			"Uses:\t equippable, reduce incoming damage by %d points\n",
			this.defPoint
		);

		return lines;
	}

	/**
	 * ApparelItem: returns defense point
	 * 
	 * @return apparel defense point
	 */
	public Integer use() {
		return this.defPoint;
	}
}