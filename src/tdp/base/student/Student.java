/* TODO: eventually move all print to somewhere else */
package tdp.base.student;

import java.util.*;
import tdp.base.combat.*;
import tdp.base.item.*;

public class Student implements Combatant {
	// general health
	protected boolean alive;
	protected int maxHealth;
	protected int health; 
	protected int healthUp;

	// mana/magic in other places
	protected int maxFocus;
	protected int focus;
	protected int focusUp;

	// exp and combat related stuff
	protected float exp;
	protected int expCap;
	protected int level;
	protected int kills;

	// attributes
	protected StudentAttributes attrib;
	protected StudentAttributes bonusAttrib;
	protected boolean baseAttribSet;

	// character name
	protected String name;
	protected String style;
	protected String iconResource;
		// actually path to icon resource e.g. "/tdp/asset/xxxxx.png"

	// character inventory
	protected Bag bag;
	protected Item hand;
	protected ApparelItem activeApparel;
	protected GearItem activeGear;

	/**
	 * Student constructor<p>
	 * initialize health/focus value to max<p>
	 * inventory size defaults to 5 slots
	 * 
	 * @param name this student's (character) name
	 * @param initHealth max health
	 * @param initFocus max focus
	 */
	public Student(String name, int initHealth, int initFocus) {
		// defaults inventory to 5 slots
		this(name, initHealth, initFocus, 5);
	}

	/**
	 * Student constructor<p>
	 * initialize health/focus value to max, inventory size to specified
	 * 
	 * @param name this student's (character) name
	 * @param initHealth max health
	 * @param initFocus max focus
	 * @param maxSlots inventory size (max slots)
	 */
	public Student(String name, int initHealth, int initFocus, int maxSlots) {
		this.alive = true;
		this.maxHealth = initHealth;
		this.health = initHealth;
		this.healthUp = 5;
		
		this.maxFocus = initFocus;
		this.focus = initFocus;
		this.focusUp = 5;

		this.level = 1;
		this.exp = 0;
		this.expCap = getExpCapByLevel(this.level);
		this.kills = 0;
		
		this.attrib = new StudentAttributes();
		this.bonusAttrib = new StudentAttributes(0, 0, 0, 0);
		this.baseAttribSet = false;

		this.bag = new Bag(maxSlots);
		this.hand = null;
		this.activeApparel = null;
		this.activeGear = null;
		
		this.name = name;
		this.style = "Reserve";
		this.iconResource = null;

		System.out.println(this.name + " has joined the game.");
	}

	// copy constructor
	public Student(Student grad) {
		this.alive = grad.alive;
		this.maxHealth = grad.maxHealth;
		this.health = grad.health;
		
		this.maxFocus = grad.maxFocus;
		this.focus = grad.focus;

		this.level = grad.level;
		this.exp = grad.exp;
		this.expCap = grad.expCap;
		this.kills = grad.kills;
		
		this.attrib = grad.attrib;
		this.bonusAttrib = grad.bonusAttrib;
		this.baseAttribSet = grad.baseAttribSet;

		this.bag = grad.bag.copyBag();
		this.hand = grad.hand;
		this.activeApparel = grad.activeApparel;
		this.activeGear = grad.activeGear;
		
		this.name = grad.name;
		this.style = grad.style;
		this.iconResource = grad.iconResource;
	}

	// defensive copy to clone/recreate student
	public Student recreate() {
		return new Student(this);
	}

	public boolean isAlive() { return this.alive; }
	public int getHealth() { return this.health; }
	public int getMaxHealth() { return this.maxHealth; }
	public int getFocus() { return this.focus; }
	public int getMaxFocus() { return this.maxFocus; }
	public Float getExp() { return this.exp; }
	public int getExpCap() { return this.expCap; }
	public int getLevel() { return this.level; }
	public int getKills() { return this.kills; }
	public String getName() { return this.name; }
	public String getStyle() { return this.style; }
	public String getIconResource() { return this.iconResource; }
	public boolean isHolding() { return (this.hand != null); }
	public Item holding() { return this.hand; }
	public ApparelItem currentApparel() { return this.activeApparel; }
	public GearItem currentGear() { return this.activeGear; }
	public StudentAttributes getAttribs() { return this.attrib; }
	
	public ArrayList<Item> getInventoryList() { 
		return this.bag.getRawListings();
	}

	public boolean isBagVacant() {
		return this.bag.isVacant();
	}

	public String toString() {
		return this.name;
	}
	
	public boolean hasIcon() {
		return this.iconResource != null;
	}

	public void setIconResource(String resourcePath) {
		this.iconResource = resourcePath;
	}

	/** 
	 * make character takes damage<p>
	 * set alive flag to false when damage is greater than health
	 * 
	 * @param damage damage taken
	 */
	public void takeDamage(int damage) {
		// people died when they are killed
		if ((damage > 0) && this.alive) {
			this.health -= damage;
			System.out.printf("%s took %d damage.\n", this.name, damage);
		}
		// a body has been discovered
		if (this.health <= 0) {
			this.alive = false;
			this.health = 0;
			System.out.println("Fatal hit!..");
		}
	}

	/** 
	 * heals character<p>
	 * no overheal
	 * 
	 * @param healed amount of hp to recover
	 */
	public void healSelf(Integer healed) {
		if ((healed > 0) && this.alive) {
			this.health += healed;
			System.out.printf("%s recovered %d health.\n", this.name, healed);
			// cap healing
			if (this.health > this.maxHealth) {
				this.health = this.maxHealth;
			}
		}
	}

	/**
	 * restore focus
	 * no over restore
	 * 
	 * @param restored amount of focus to restore
	 */
	public void restoreFocus(Integer restored) {
		if ((restored > 0) && this.alive) {
			this.focus += restored;
			System.out.printf("%s restored %d focus.\n", this.name, restored);

			if (this.focus > this.maxFocus) {
				this.focus = this.maxFocus;
			}
		}
	}

	/**
	 * use focus for whatever activities
	 * 
	 * @param required the required amount of focus to succeed
	 * @return the required focus, or remaining focus if it's not enough
	 */
	public int consumeFocus(int required) {
		int availableFocus = 0;
		if ((this.focus >= required) && this.alive) {
			this.focus -= required;
			availableFocus = required;
		}
		else if (this.focus < required) {
			availableFocus = this.focus;
		}
		return availableFocus;
	}

	/**
	 * revive this character
	 * 
	 * @return true if revival succeed
	 */
	public boolean revive() {
		if (!this.alive) {
			this.alive = true;
			System.out.printf("%s got revived.\n", this.name);
			int revivedHealth = (int)(maxHealth * 2/10);
			this.healSelf(revivedHealth);
			return true;
		}

		return false;
	}

	/**
	 * makes character gain exp, do levelUp() if exceed level cap
	 * 
	 * @param pendingExp new exp point
	 * @return amount of exp gained
	 */
	public double gainExp(double pendingExp) {
		this.exp += pendingExp;
		int step = 0;

		System.out.printf("%s gained %.2f exp.\n", this.name, pendingExp);
		if (this.exp > this.expCap) {
			step = this.levelUp();
		}
		
		return pendingExp;
	}

	/**
	 * try to put an item into their bag
	 * 
	 * @param item item to put into the bag
	 * @return true if putting item is a success
	 */
	public boolean putItem(Item item) {
		boolean putSuccess = false;
		if (this.alive) {
			System.out.printf("%s puts '%s' into their bag.\n",
				this.name, item.getName());

			putSuccess = this.bag.putItem(item);
			if (!putSuccess) {
				System.out.printf("%s's bag is full...\n", this.name);
			}
		}
		return putSuccess;
	}

	public Item pullItem(String itemName) {
		Item pulled = null;
		if (this.alive) {
			pulled = this.bag.getItem(itemName);
			if (pulled != null) {
				System.out.printf("%s pulled '%s' out of their bag.\n",
					this.name, pulled.getName());
				//this.holdItem(pulled, true);
			}
			else {
				System.out.printf("%s does not have '%s' in their bag.\n",
					this.name, itemName);
			}
		}
		return pulled;
	}

	/**
	 * list items in their bag
	 */
	public void listItems() {
		System.out.printf(" **** %s (%s) bag: %d items **** \n", 
			this.name, this.style, this.bag.size());
			
		if (this.bag.size() > 0) {
			this.bag.listItems();
		}

		if (this.hand != null) {
			System.out.printf("%s is holding:\n", this.name);
			this.hand.describe();
		}
	}
	
	/**
	 * look for an item with given name and report back
	 * 
	 * @param itemName an item name to look for
	 * @return true if such item exists
	 */
	public boolean searchItem(String itemName) {
		int count = 0;
		if (this.alive) {
			count = this.bag.searchInBag(itemName);
			if (count > 0) {
				System.out.printf("%s has %d of '%s' in their bag.\n", 
					this.name, count, itemName);
			}
			else {
				System.out.printf("%s does not have '%s' in the bag.\n", 
					this.name, itemName);
			}
		}
		return (count > 0);
	}

	/**
	 * attempts to use an item. using equipable items will equip them in 
	 * appropiate slots, unequipping old gear if necessary.
	 * using a consumable item will consume them for whatever effect
	 * 
	 * @param item item to be used
	 * @return bool indicating using an item is a success
	 */
	public boolean useItem(Item item) {
		ItemCategory it = item.getCatagory();
		boolean useSuccess = false;
		switch (it) {
			case CONSUMABLE:
				useSuccess = this.consumeItem(item);
				break;
				
			case APPAREL:
				if (this.activeApparel != null) {
					this.unequipApparel();
				}
				if (this.activeApparel == null) {
					this.activeApparel = (ApparelItem)item;
					System.out.printf(
						"%s equipped '%s' as their apparel.\n",
						this.name, this.activeApparel.getName());
					//this.discardHand();
						// NOTE: DO discard hand by outsider
					useSuccess = true;
				}
				break;
			
			case GEAR:
				if (this.activeGear != null) {
					this.unequipGear();
				}
				if (this.activeGear == null) {
					this.activeGear = (GearItem)item;
					System.out.printf(
						"%s equipped '%s' as their gear.\n",
						this.name, this.activeGear.getName());
					//this.discardHand();
					useSuccess = true;
				}
				break;
			
			default:
				System.out.printf("%s decided to inspect the item...\n",
					this.name);
				item.use();
				useSuccess = true;
				break;
		}	
	
		return useSuccess;
	}

	/**
	 * consume/equip an item directly from bag <p>
	 * a combination of pull + use
	 * 
	 * @param itemName
	 * @return true if usage item success
	 */
	public boolean useItemFromBag(String itemName) {
		boolean success = false;
		Item toUse = this.pullItem(itemName);
		if (toUse != null) {
			if ((toUse instanceof GearItem)
				|| (toUse instanceof ApparelItem)
				|| (toUse instanceof ConsumableItem)) {
				success = this.useItem(toUse);
			}
			if (! success) {
				System.out.printf(
					"%s refused to use '%s'\n", 
					this.name, toUse.getName());
			}
		}
		return success;
	}

	public boolean consumeItem(Item item) {
		boolean useSuccess = false;
		if (item instanceof ConsumableItem) {
			ConsumableItem c = (ConsumableItem)item;

			switch (c.getRecoveryType()) {
				case INFLUENCE: {
					if (this.health < this.maxHealth) {
						this.healSelf(item.use());
						// item is consumed, forcibly discard
						//this.discardHand();
						useSuccess = true;
					}
					break;
				}
				case FOCUS: {
					if (this.focus < this.maxFocus) {
						this.restoreFocus(item.use());
						//this.discardHand();
						useSuccess = true;
					}
					break;
				}
				case BOTH: {
					if (this.health < this.maxHealth 
						|| this.focus < this.maxFocus) {
						this.healSelf(item.use());
						this.restoreFocus(item.use());
						//this.discardHand();
						useSuccess = true;
					}
					break;
				}
			}
		}
		return useSuccess;
	}
	/**
	 * unequip apparel item and return it to the bag
	 * or hold it<p>
	 * wrapper of unequipItem()
	 * 
	 * @return unequipped apparel item
	 * @see unequipItem
	 */
	public Item unequipApparel() {
		return this.unequipItem(ItemCategory.APPAREL);
	}

	/**
	 * unequip gear item and return it to the bag
	 * or hold it<p>
	 * wrapper of unequipItem()
	 * 
	 * @return unequipped gear item
	 * @see unequipItem
	 */
	public Item unequipGear() {
		return unequipItem(ItemCategory.GEAR);
	}

	private Item unequipItem(ItemCategory itc) {
		boolean ueqSuccess = false;
		Item unequipped = null;
			// pull item out of slots
		switch (itc) {
			case APPAREL:
				unequipped = this.activeApparel;
				this.activeApparel = null;
				break;
			case GEAR:
				unequipped = this.activeGear;
				this.activeGear = null;
				break;
			default:
				return unequipped;
					// will be null
		}
		// put it back into bag
		ueqSuccess = this.putItem(unequipped);
		
		// put back into bag fails
		// if hand is free, hold it
		if (!(ueqSuccess || this.isHolding())) {
			ueqSuccess = this.holdItem(unequipped, false);
		}

		// unequip fails, reequip 
		if (!ueqSuccess) {
			this.useItem(unequipped);
			unequipped = null;
		}
		
		return unequipped;
	}

	/**
	 * hold an item in their hands
	 * try not to discard item if one is already held.
	 * 
	 * @param item an item to hold
	 * @param forceDiscardHand if set, discard  holding item
	 * @return true if item on hand is changed
	 */
	public boolean holdItem(Item item, boolean forceDiscardHand) {
		if (forceDiscardHand) {
			this.discardHand();
		}
		if (this.hand == null) {
			this.hand = item;
			System.out.printf("%s hold '%s' in their hand.\n", 
				this.name, this.hand.getName());
			return true;
		}
		return false;
	}

	/**
	 * discard currently holding item
	 * 
	 * @return true if item on hand is discarded, false when hand is empty
	 */
	public boolean discardHand() {
		if (this.alive) {
			if (this.hand != null) {
				this.hand = null;
				return true;
			}
		}
		return false;
	}

	/**
	 * trade items between students<p>
	 * trade from hand to their bag
	 * 
	 * @param target Student to trade with
	 * @return true if items has been traded
	 */
	public boolean tradeItems(Student target) {
		//Item temporary;
		boolean success = false;
		if (this.isHolding() && target.bag.isVacant()) {
			target.putItem(this.holding());
			this.discardHand();
			success = true;
		}
		if (target.isHolding() && this.bag.isVacant()) {
			this.putItem(target.holding());
			target.discardHand();
			success = true;
		}
		return success;
	}

	public void listEquipments() {
		System.out.printf(" **** %s (%s) equipments **** \n", 
			this.name, this.style);
		if (this.activeApparel != null) {
			this.activeApparel.describe();
		}
		else {
			System.out.println("Apparel:None");
		}
		System.out.println();
		if (this.activeGear != null) {
			this.activeGear.describe();
		}
		else {
			System.out.println("Gear:\tNone");
		}
	}

	/**	
	 * prints all status
	 */
	public void statusCheck() {
		System.out.printf(" **** %s (%s) status **** \n", 
			this.name, this.style);
		System.out.printf("Influence:\t%5d /%5d%s\n", 
			this.health, this.maxHealth, (this.alive? "": "(down)"));
		System.out.printf("Focus:\t\t%5d /%5d\n", 
			this.focus, this.maxFocus);
		System.out.printf("Items:\t\t%5d /%5d\n", 
			this.bag.size(), this.bag.getSlots());
		System.out.printf("Level:\t\t%5d\n", this.level);
		System.out.printf("Exp:\t\t%5.2f /%5d\n", 
			this.exp, this.expCap);
	}

	/**
	 * level up a character<p>
	 * rapidly level up until you can't<p>
	 * inspired by ngress level system where exp is retained but
	 * the cap is moved
	 * 
	 * @return level up steps
	 */
	protected int levelUp() {
		int levelUpStep = 0;
		while (this.exp > this.expCap) {
			levelUpStep += 1;
			this.level += 1;
			this.maxHealth += this.healthUp;
			this.health += this.healthUp;
			this.maxFocus += this.focusUp;
			this.focus += this.focusUp;
			this.expCap = getExpCapByLevel(level);
			System.out.printf("%s Level Up! now L%d\n",
				this.name, this.level);
			this.attrib.addStats(1, 1, 1, 1);
		}
		this.attrib.addDelta(this.bonusAttrib);
		return levelUpStep;
	}

	/**
	 * set how much more influence/focus a student gets per levelup
	 * (like one level up yields 5 more max health, SportStudent can set
	 * heath step to be higher)
	 * @param healthStep max health step value per level up
	 * @param focusStep max focus step value per level up
	 */
	protected void setLevelUpStep(int healthStep, int focusStep) {
		this.healthUp = healthStep;
		this.focusUp = focusStep;
	}

	/**
	 * set base attributes for this student <p>
	 * can only be used once (in initialization)
	 * @param attrib
	 */
	public void setBaseAttrib(StudentAttributes attrib) {
		if (!this.baseAttribSet) {
			this.attrib = attrib;
			this.baseAttribSet = true;
		}
	}

	/**
	 * set extra stat improvement point when leveling up
	 * 
	 * @param bonus StudentAttributes delta to be applied when level up
	 */
	protected void setBonusAttrib(StudentAttributes bonus) {
		this.bonusAttrib = bonus;
	}

	/**
	 * swap current bag with other bag
	 * meant to be used with Remnants
	 * 
	 * @param newBag a bag to swap
	 * @return previous bag of this student
	 */
	protected Bag swapBag(Bag newBag) {
		Bag oldBag = this.bag;
		this.bag = newBag;
		return oldBag;
	}

	/** 
	 * helper function to calculate exp required to level up<p>
	 * inspired by ingress exp system (moving goalpost)
	 * 
	 * @param level level to determine exp cap
	 * @return exp cap for a given level
	 */
	private int getExpCapByLevel(int level) {
		double expMultiple = 25;
		double expFactor = 1.5;
		double exactExpCap = (expMultiple * Math.pow(level, expFactor));
		double expUpperCap = expMultiple 
			* (Math.ceil(exactExpCap / expMultiple));
		double expLowerCap = expMultiple 
			* (Math.floor(exactExpCap / expMultiple));
		
		if(Math.abs(expLowerCap - exactExpCap) 
			> Math.abs(expUpperCap - exactExpCap)) {
			return (int)expLowerCap;
		}
		else {
			return (int)expUpperCap;
		}
	}

	public boolean performAttack(Student target) {
		boolean result = target.resolveAttack(0, this);
		return result;
	}

	public boolean resolveDirectAttack(int damage, Student attacker) {
		boolean atkSuccess = false;
		if (damage > 0) {
			this.takeDamage(damage);
			atkSuccess = true;
		}

		if (!this.alive && atkSuccess) {
			this.resolveDeaths(attacker);
		}
		
		return atkSuccess;
	}

	public boolean resolveAttack(int oroll, Student attacker) {
		boolean atkSuccess = false;
		Random rng = new Random();
		int roll = 0;
		if (oroll == 0) {
			roll = rng.nextInt(12) + 1;
		}
		else if (oroll > 0) {
			roll = oroll;
		}

		int damage = roll
			+ attacker.getDamageModifier()
			- this.getDefenseModifier();

		if (damage > 0 && this.alive) {
			this.takeDamage(damage);
			atkSuccess = true;
		}

		if (!this.alive && atkSuccess) {
			this.resolveDeaths(attacker);
		}

		return atkSuccess;
	}

	private void resolveDeaths(Student attacker) {
		// count
		attacker.kills += 1;

		// loot 
		Item loot = this.bag.pullRandomItem();
		if (loot != null) {
			attacker.putItem(loot);
		}

		// exp
		double statSum = this.attrib.getIntel() + this.attrib.getStrength()
			+ this.attrib.getSpeed() + this.attrib.getLuck();
		double pendingExp = this.level * (statSum);

		attacker.gainExp(pendingExp);
	}

	public int getDamageModifier() {
		int baseStr = attrib.getStrength();
		int gearMod = 0;
		
		if (this.activeGear != null) {
			gearMod = this.activeGear.getAttackPower();
		}

		return baseStr + gearMod;
	}

	public int getDefenseModifier() {
		int baseStr = attrib.getStrength();
		int apparelMod = 0;
		
		if (this.activeApparel != null) {
			apparelMod = this.activeApparel.getDefPoint();
		}

		return baseStr + apparelMod;
	}
}