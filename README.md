# tdp

Part of 240-210 Programming Technique 61 class

definitely was not inspired by/based on ragnarok online
so many things will look off.

use [`BagTesterInteractive`][intrbagtest] to test various functions, as of now

[`ant`][ant] simple buildfile is provided.

## Instructions

1. get [`ant`][ant]
2. `ant build`
3. `cd build/`
4. `java PartyUI`

## Rough outline of Implemented features

- [x] [Student][student] (base character aka novice)
- [x] [Bag](src/tdp/base/Bag.java) and [Item][item]
  - [x] dead simple tester via [BagTester](src/BagTester.java)
  - [x] a more complex tester [BagTesterInteractive][intrbagtest]
  - [x] rudimentary item use/equipping system
  - [ ] complete item use system
- [x] extending typed [item][item] into their own class
  - [x] [gear](src/tdp/base/item/GearItem.java)
  - [x] [apparel](src/tdp/base/item/ApparelItem.java)
  - [x] [consumable](src/tdp/base/item/ConsumableItem.java)
  - [ ] material
- [ ] extending [students][student] into their own class
  - [x] [sport](src/tdp/base/student/SportStudent.java)
  - [x] [intellect](src/tdp/base/student/IntellectStudent.java)
  - [ ] variety
  - [x] simple battle system
  - [x] simple inventory system
- [x] UI
  - [x] main/party/inventory screen
  - [x] battle screen
- [ ] something else

[ant]: https://ant.apache.org/
[intrbagtest]: src/BagTesterInteractive.java
[student]: src/tdp/base/student/Student.java
[item]: src/tdp/base/item/Item.java