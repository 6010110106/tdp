package tdp.base.remnant;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;

import javax.imageio.ImageIO;

import tdp.base.student.*;
import tdp.base.combat.BattleAction;
import tdp.base.combat.NewBattleAction;
import tdp.base.item.*;
import tdp.asset.*;

public class RemnantMastermind {
	private Random rng;
	private ItemServer its;

	private String[] class77 = {
		"Akane Owari", "Fuyuhiko Kuzuryu", 
		"Gundam Tanaka", "Hiyoko Saionji", 
		"Ibuki Mioda", "Byakuya Togami", 
		"Kazuichi Souda", "Mahiru Koizumi", 
		"Mikan Tsumiki", "Nagito Komaeda", 
		"Nekomaru Nidai", "Peko Pekoyama", 
		"Sonia Nevermind", "Teruteru Hanamura"
	};

	public RemnantMastermind() {
		this.rng = new Random();
		this.its = new ItemServer();
	}

	/**
	 * generating a Remnant, with some student as reference point
	 * to make it more even
	 * 
	 * @param refs student as a reference for remnant strength
	 * @return a remnant with random names and other 
	 */
	public Remnant birthRemnant(Student refs) {
		int healthOffset = rng.nextInt(11) - 5;
		int focusOffset = rng.nextInt(11) - 5;

		// random name and stats
		Remnant remnant = new Remnant(
			this.getRandomName(),
			refs.getMaxHealth() + healthOffset, 
			refs.getMaxFocus() + focusOffset
		);

		remnant.setIconResource(
			String.format(
				"/tdp/asset/icons/%s.png", 
				remnant.getName().split(" ", 2)[1]
			)
		);

		// beef em up
		double expRange = refs.getExp() * 0.2;
		double expOffset = (rng.nextDouble()-0.5) * expRange;
		remnant.gainExp(refs.getExp() + expOffset);

		StudentAttributes randomAttrib = new StudentAttributes(
			refs.getAttribs().getSpeed() + rng.nextInt(3),
			refs.getAttribs().getStrength() + rng.nextInt(3),
			refs.getAttribs().getIntel() + rng.nextInt(3),
			refs.getAttribs().getLuck() + rng.nextInt(3)
		);
		remnant.setBaseAttrib(randomAttrib);

		// give em stuff (gear, apparel, consumable, extra)
		remnant.putItem(
			this.its.getRandomItemByCategory(ItemCategory.GEAR)
		);
		remnant.putItem(
			this.its.getRandomItemByCategory(ItemCategory.APPAREL)
		);
		remnant.putItem(
			this.its.getRandomItemByCategory(ItemCategory.CONSUMABLE)
		);
		remnant.putItem(this.its.getRandomItem());

		// maybe some equipment too, if refs has some
		if (refs.currentApparel() != null && this.rng.nextDouble() < 0.5) {
			remnant.useItem(
				this.its.getRandomItemByCategory(ItemCategory.APPAREL)
			);
		}
		if (refs.currentGear() != null  && this.rng.nextDouble() < 0.5) {
			remnant.useItem(
				this.its.getRandomItemByCategory(ItemCategory.GEAR)
			);
		}

		return remnant;
	}

	public java.net.URL validateIconRes(Remnant remnant) {
		String iconRes = remnant.getIconResource();
		java.net.URL imgURL = getClass().getResource(iconRes);
		return imgURL;
	}

	public NewBattleAction commandRemnant(Remnant remnant, Student student) {
		Student target = student;
		String actCat = "attack";
		double healthRatio = remnant.getHealth()*1.0 / remnant.getMaxHealth();

		double rand = rng.nextDouble();
		// might idle
		if (rand < 0.05) {
			target = null;
		}
		// try to flee when low health
		if (rand < 0.5 && healthRatio < 0.2) {
			//System.out.println(healthRatio);
			actCat = "flee";
		}
		NewBattleAction remnantAct = 
			new NewBattleAction(remnant, target, actCat);
		
		return remnantAct;
	}

	private String getRandomName() {
		int index = rng.nextInt(class77.length);
		return class77[index];
	}
}