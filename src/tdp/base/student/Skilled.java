package tdp.base.student;

public interface Skilled {
	public void primarySkill(Student target);
	public void secondarySkill(Student target);
	public void auxillarySkill(Student target);
}
