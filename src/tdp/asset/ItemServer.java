package tdp.asset;

import java.util.*;
import tdp.base.item.*;

public class ItemServer {
	public ArrayList<Item> itemServer;
	private ArrayList<Item> regularItems;
	private ArrayList<Item> consumeItems;
	private ArrayList<Item> gearItems;
	private ArrayList<Item> apparelItems;


	public ItemServer() {
		this.itemServer = new ArrayList<>();
		this.regularItems = new ArrayList<>();
		this.consumeItems = new ArrayList<>();
		this.gearItems = new ArrayList<>();
		this.apparelItems = new ArrayList<>();
		
		// register some items in the itemServer
		// TODO: implement parser so it can read from file instead of this madness
		
		// Regular Items
		this.regularItems.add(
			new Item(
				"Knife", 
				"It has a glowing paint stain on it."
			)
		);
		this.regularItems.add(
			new Item(
				"A Man's Fantasy",
				"It urges the holder to seek out a true man's fantasy."
			)
		);

		// Consumables
		this.consumeItems.add(
			new ConsumableItem(
				"Face Shaped Cookie",
				"You swear you've seen this face somewhere before",
				5
			)
		);
		this.consumeItems.add(
			new ConsumableItem(
				"Panta",
				"A favorite drink of a certain liar.", 
				8, 
				ConsumeType.FOCUS
			)
		);
		this.consumeItems.add(
			new ConsumableItem(
				"Ration",
				"taste decent, certain snakes that enjoy "
				+ "hide-and-go-seek are just crazy about it.",
				16,
				ConsumeType.BOTH
			)
		);
		this.consumeItems.add( 
			new ConsumableItem(
				"Astro Cake",
				"Freeze-dired cake sold as space cakes. quite healthy.",
				12,
				ConsumeType.BOTH
			)
		);

		// apparel
		this.apparelItems.add(
			new ApparelItem(
				"Galactic Jacket", 
				"A peculiar jacket with the lining of the universe.",
				5
			)
		);
		this.apparelItems.add(
			new ApparelItem(
				"Ultimate uniform", 
				"Despite the name, it's just a regular apparel",
				4
			)
		);
		this.apparelItems.add(
			new ApparelItem(
				"Bondage Boots",
				"Enamel and chain boots fit for a queen.",
				3
			)
		);

		// gear
		this.gearItems.add(
			new GearItem(
				"Stainless tray",
				"A silver tray shines like a mirror, "
				+ "still hard enough to slap someone and knock them out.",
				6
			)
		);
		this.gearItems.add(
			new GearItem(
				"Golden Gun", 
				5
			)
		);
		this.gearItems.add(
			new GearItem(
				"Megaphone",
				"Does not actually amplify sounds but it works as a weapon.",
				4
			)
		);
		this.gearItems.add(
			new GearItem(
				"Katana",
				6
			)
		);

		this.itemServer.addAll(this.regularItems);
		this.itemServer.addAll(this.consumeItems);
		this.itemServer.addAll(this.gearItems);
		this.itemServer.addAll(this.apparelItems);
	}

	public ArrayList<Item> getList() { return this.itemServer; }

	/**
	 * randomly selected an item from itemServer, can select where to look from
	 * @param itemPool pool (ArrayList<Item>) of items to draw form
	 * @return a randomly selected Item in given pool
	 */
	public Item getRandomItem(ArrayList<Item> itemPool) {
		Random rng = new Random();
		int rngnum = rng.nextInt(itemPool.size());
		Item generated = itemPool.get(rngnum);
		return generated;
	}

	/**
	 * randomly selected an item from itemServer
	 * @return a randomly selected Item in given pool
	 */
	public Item getRandomItem() {
		return this.getRandomItem(this.itemServer);
	}

	/**
	 * gives a random item based on specified category, defaults to all items
	 * in the server.
	 * @param it item category
	 * @return randomly selected item from specified category
	 */
	public Item getRandomItemByCategory(ItemCategory it) {
		ArrayList<Item> itemPool;
		switch (it) {
			case REGULAR:
				itemPool = this.regularItems;
				break;
			case CONSUMABLE:
				itemPool = this.consumeItems;
				break;
			case APPAREL:
				itemPool = this.apparelItems;
				break;
			case GEAR:
				itemPool = this.gearItems;
				break;
			default:
				itemPool = this.itemServer;
		}
		return this.getRandomItem(itemPool);
	}
}